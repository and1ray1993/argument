export const slider = (path, name) => {
  const slide = document.querySelectorAll(".portfolio-item"),
    portfolioDots = document.querySelector(".portfolio-dots"),
    slider = document.querySelector(".portfolio-content");


  for (let i = 0; i < slide.length; i++) {
    let img = document.createElement("img");
    img.setAttribute('src', path + name + '/' + (i + 1) + '.jpeg')
    let dotForSlider = document.createElement("li");
    dotForSlider.classList.add("dot");
    dotForSlider.appendChild(img);
    if (i === 0) {
      dotForSlider.classList.add("dot-active");
    }
    portfolioDots.appendChild(dotForSlider);
  }

  const dot = document.querySelectorAll(".dot");

  let currentSlide = 0;

  const prevSlide = (elem, index, strClass) => {
    elem[index].classList.remove(strClass);
  };

  const nextSlide = (elem, index, strClass) => {
    elem[index].classList.add(strClass);
  };

  const playSlide = () => {
    prevSlide(slide, currentSlide, "portfolio-item-active");
    prevSlide(dot, currentSlide, "dot-active");

    currentSlide++;

    if (currentSlide >= slide.length) {
      currentSlide = 0;
    }

    nextSlide(slide, currentSlide, "portfolio-item-active");
    nextSlide(dot, currentSlide, "dot-active");
  };

  slider.addEventListener("click", (event) => {
    event.preventDefault();

    let target = event.target;

    if (!target.matches(".portfolio-btn, .dot")) {
      return;
    }

    prevSlide(slide, currentSlide, "portfolio-item-active");
    prevSlide(dot, currentSlide, "dot-active");

    if (target.matches("#arrow-right")) {
      currentSlide++;
    } else if (target.matches("#arrow-left")) {
      currentSlide--;
    } else if (target.matches(".dot")) {
      dot.forEach((elem, index) => {
        if (elem === target) {
          currentSlide = index;
        }
      });
    }

    if (currentSlide >= slide.length) {
      currentSlide = 0;
    } else if (currentSlide < 0) {
      currentSlide = slide.length - 1;
    }

    nextSlide(slide, currentSlide, "portfolio-item-active");
    nextSlide(dot, currentSlide, "dot-active");
  });

  let event = null;
  let delta;

  slider.addEventListener("touchstart", function (e) {
    event = e;
  });
  slider.addEventListener("touchmove", function (e) {
    if (event) {
      delta = e.touches[0].pageX - event.touches[0].pageX;
    }
  });
  slider.addEventListener("touchend", function (e) {
    console.log(delta);

    prevSlide(slide, currentSlide, "portfolio-item-active");
    prevSlide(dot, currentSlide, "dot-active");

    if (delta < -60) {
      currentSlide++;
    } else if (delta > 60) {
      currentSlide--;
    }

    if (currentSlide >= slide.length) {
      currentSlide = 0;
    } else if (currentSlide < 0) {
      currentSlide = slide.length - 1;
    }

    nextSlide(slide, currentSlide, "portfolio-item-active");
    nextSlide(dot, currentSlide, "dot-active");

    event = null;
  });
  playSlide();
};
