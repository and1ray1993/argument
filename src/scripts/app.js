import {slider} from './slider';
import {disableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock';

export const app = (data) => {

  Vue.use(window.vuelidate.default);
  Vue.use(VueMask.VueMaskPlugin);

  const {
    required
  } = validators;
  const {
    email
  } = validators;
  const {
    minLength
  } = validators;

  let vm = new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    data: {
      catalog: data,
      cardData: '',
      quizActive: false,
      callMeActive: false,
      cardActive: false,
      thanksActive: false,
      showMenu: false,
      detail: false,
      conf: false,
      bufferName: '',
      mask: '+###(##)###-##-##',
      quizItem: 0,
      tab: 1,
      quizData: [],
      email: '',
      phone: '',
      name: '',
      gun: '',
      comment: '',
    },
    methods: {
      changeClass(item) {
        return {
          'installment-plan': item.Installment != 'null' && item.Installment,
          'accessories-mod': item.Category == 'Аксессуары'
        }
      },
      getActiveCard(event) {
        const item = $('.js-quiz-item');
        const card = $(event.target);
        item.each((i, el) => {
          $(el).attr('data-active', 'false');
        });
        card.attr('data-active', 'true');
        this.quizData[this.quizItem] = card.find('.quiz__item-text').text();
      },
      reseultCardQuiz() {
        const item = $('.js-quiz-item');
        item.each((i, el) => {
          if ($(el).attr('data-active') === 'true') {
            this.quizData[this.quizItem] = $(el)
            .find('.quiz__item-text')
            .text();
            this.quizItem++;
          }
        });
      },
      get_file_url(url) {

        let link_url = document.createElement("a");

        link_url.download = url.substring((url.lastIndexOf("/") + 1),
          url.length);
        link_url.href = url;
        document.body.appendChild(link_url);
        document.body.removeChild(link_url);
      },
      sendForm(event) {

        let data = {
          'email': this.email,
          'phone': this.phone,
          'name': this.name,
          'quiz': this.quizData,
          'comment': this.comment,
          'product': this.cardData.Name
        };

        if ($(event.target).attr('href') === '#') {
          event.preventDefault();
        }

        if ($(event.target).attr('href') == undefined || $(event.target).attr('href') !== '#') {
          if ($(event.target).attr('href') != undefined) {
            this.get_file_url($(event.target).attr('href'));
          }
          $.ajax({
            url: 'send.php',
            type: 'POST',
            dataType: 'json',
            data: "param=" + JSON.stringify(data)
          });
        }

        $(event.target).hasClass('js-thanks') ?
          (this.thanksActive = true) && (this.cardActive = false) :
          (this.quizActive = true);

      },
      closePopups() {
        this.quizActive = false;
        this.callMeActive = false;
        this.cardActive = false;
        this.thanksActive = false;
        this.detail = false;
        this.conf = false;

        this.$v.$reset();
        this.email = '';
        this.phone = '';
        this.name = '';
        this.quizItem = 0;

        clearAllBodyScrollLocks();
      },
      scrollToCatalog(num) {
        $('html, body').animate({
          scrollTop: $('.catalog').offset().top
        }, 1700);
        if (num) {
          this.tab = num;
          $('.catalog__tab').each((i, item) => {
            $(item).removeClass('active');
            if (+$(item).attr('data-index') === num) {
              $(item).addClass('active');
            }
          });
        }
      },
      scrollToQuiz() {
        $('html, body').animate({
          scrollTop: $('.quiz__second-title').offset().top
        }, 1700);
      },
      activeTab(num, event) {
        if (!$(event.target).hasClass('active')) {
          $('.catalog__tab').each((i, item) => {
            $(item).removeClass('active');
          });
          $(event.target).addClass('active');
        }
        this.tab = num;
      },
      popupActive(item) {
        this.cardActive = true;
        this.cardData = item;
        disableBodyScroll($('.popup-card'));
        if (this.detail) this.detail = false;
      },
      getSliderSettings() {
        return {
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      activeDetails(item) {
        this.detail = true;
        this.cardData = item;

        this.$nextTick(function () {
          slider(this.cardData.PathToSlides, this.cardData.Name)
          disableBodyScroll($('.popup-detail'));
        })
      }
    },
    validations: {
      email: {
        email,
        required,
      },
      phone: {
        required,
      },
      name: {
        required,
        minLength: minLength(2),
      },
    },
    filters: {
      clearLength: function (value) {
        value = value.toString();
        if (value.length > 4) {
          return value.substring(0, 4);
        }
        return value;
      }
    }
  });
}