import array from 'lodash/array'
import {app} from './app'

$(document).ready(() => {

  $.ajax({
    url: "assets/other/convert.php",
    cache: false,
    success: function (html) {
      console.log('convert success');
    }
  });

  window.data = []
  window.onDeveloper = () => console.log('Andrey Petukhov');


  $.getJSON('assets/other/catalog.json', (data) => {

    for (let k = 0; k < data.Worksheet.length; k++) {
      window.data[k] = [];
      let list = [];
      $(data.Worksheet[k].Table.Row).each((i, item) => {
        list[i] = [];
        for (let key in item) {
          $(item[key]).each((j, el) => {
            list[i].push(el.Data);
          })
        }
      });
      for (let index = 1; index < list.length; index++) {
        window.data[k].push(array.zipObject(list[0], list[index]))
      }
    }
    app(window.data)
  });
});
