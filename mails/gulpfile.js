'use strict';

const autoprefixer = require('autoprefixer');
const browserSync  = require('browser-sync').create();
const gulp         = require('gulp');
const imagemin     = require('gulp-imagemin');
const inlineCss    = require('gulp-inline-css');
const notify       = require('gulp-notify');
const plumber      = require('gulp-plumber');
const postcss      = require('gulp-postcss');
const sass         = require('gulp-sass');
const sassGlob     = require('gulp-sass-glob');
const twig         = require('gulp-twig');

// Обработчик ошибок
const handleError = err => {
  notify.onError({
    title:   'Gulp error',
    message: err.message
  })(err);
};

// Сервер
gulp.task('server', () => {
  browserSync.init({
    server: {
      baseDir: 'build/'
    },
    host:   'localhost',
    port:   9001,
    notify: false
  });
});
gulp.task('server:refresh', () => {
  browserSync.reload();
});
gulp.task('server:inject', () => {
  gulp.src('build/styles/**/*.*')
    .pipe(browserSync.stream());
});

// Билды
gulp.task('build:styles', () => {
  gulp.src('src/styles/*.scss')
    .pipe(plumber(handleError))
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 2 version'],
        grid:     true
      })
    ]))
    .pipe(gulp.dest('build/styles/'));
});
gulp.task('build:html', () => {
  gulp.src([
    'src/pages/*.twig',
    'src/pages/*.html'
  ])
    .pipe(twig())
    .pipe(inlineCss({
      removeStyleTags: false
    }))
    .pipe(gulp.dest('build/'));
});
gulp.task('build:assets', () => {
  gulp.src('src/assets/fonts/**/*.*')
    .pipe(gulp.dest('build/assets/fonts/'));

  gulp.src('src/assets/other/**/*.*')
    .pipe(gulp.dest('build/assets/other/'));

  gulp.src('src/assets/img/**/*.*')
    .pipe(imagemin([
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {convertStyleToAttrs: true},
          {removeUnknownsAndDefaults: true},
          {cleanupIDs: true},
          {cleanupAttrs: true},
          {inlineStyles: true},
          {collapseGroups: true},
          {removeMetadata: true},
          {minifyStyles: true}
        ]
      })
    ]))
    .pipe(gulp.dest('build/assets/img/'));
});

// Вотчеры
gulp.task('watch:build', ['server', 'build:html', 'build:styles', 'build:assets'],
  () => {
    gulp.watch([
        'src/pages/**/*.twig',
        'src/pages/**/*.html',
        'src/blocks/**/*.twig',
        'src/blocks/**/*.html'
      ],
      ['build:html']);
    gulp.watch([
        'src/styles/**/*.*',
        'src/blocks/**/*.scss'
      ],
      ['build:styles']);
    gulp.watch('src/assets/**/*.*', ['build:assets']);
  });
gulp.task('watch:update', () => {
  gulp.watch([
    'build/**/*.*',
    '!build/styles/**/*.*'
  ], ['server:refresh']);

  gulp.watch(['build/styles/**/*.*', 'src/styles/**/*.*', 'src/blocks/**/*.scss'] ['server:inject']);
});

gulp.task('default', ['watch:build', 'watch:update']);
gulp.task('build', ['build:html', 'build:styles', 'build:assets', 'build:inline']);
